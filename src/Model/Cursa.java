package Model;

import java.io.Serializable;

public class Cursa implements Serializable {

    private int pret;
    private String data;
    private String plecare;
    private String sosire;

    public String getDurata() {
        return durata;
    }

    public void setDurata(String durata) {
        this.durata = durata;
    }

    private String durata;
    private Avion avion;

    public Cursa(int pret, String data, String plecare, String sosire, String durata, Avion avion) {
        this.pret = pret;
        this.data = data;
        this.plecare = plecare;
        this.sosire = sosire;
        this.durata = durata;
        this.avion = avion;
    }

    public void setCursa(Cursa x){
        pret=x.getPret();
        data=x.getData();
        plecare=x.getPlecare();
        sosire=x.getSosire();
        durata=x.getDurata();
        avion=x.getAvion();
    }
    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPlecare() {
        return plecare;
    }

    public void setPlecare(String plecare) {
        this.plecare = plecare;
    }

    public String getSosire() {
        return sosire;
    }

    public void setSosire(String sosire) {
        this.sosire = sosire;
    }

    public Avion getAvion() {
        return avion;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    @Override
    public String toString() {
        return "Cursa{" +
                "pret=" + pret +
                ", data='" + data + '\'' +
                ", plecare='" + plecare + '\'' +
                ", sosire='" + sosire + '\'' +
                ", avion=" + avion.getLinieAeriana() +
                '}';
    }



    public String[] vizualizareOrar(){
        return new String[]{getData(),getPlecare(),getSosire(),getDurata(),getAvion().getLinieAeriana(),getAvion().getNume(),Integer.toString(getAvion().getNumar())};
    }

    public String[] vizualizareOrar2(){
        return new String[]{Integer.toString(pret),data,plecare,sosire,durata,Integer.toString(avion.getNumar()),Integer.toString(avion.getNrLocuri()),getAvion().getNume(),avion.getLinieAeriana()};
    }

    public String[] vizualizareAngajat() {
        return new String[]{Integer.toString(getPret()), getData(), getPlecare(), getSosire(), getDurata(), Integer.toString(getAvion().getNumar()), Integer.toString(getAvion().getNrLocuri()), getAvion().getNume(), getAvion().getLinieAeriana()};
    }
}
