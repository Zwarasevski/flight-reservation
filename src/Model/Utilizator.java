package Model;

import java.io.Serializable;

public class Utilizator implements Serializable {

    private String tipUtilizator;
    private String nume;
    private boolean reducere;
    private String email;
    private String nrTelefon;

    public Utilizator(String tipUtilizator, String nume, boolean reducere, String email, String nrTelefon) {
        this.tipUtilizator = tipUtilizator;
        this.nume = nume;
        this.reducere = reducere;
        this.email = email;
        this.nrTelefon = nrTelefon;
    }


    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public boolean isReducere() {
        return reducere;
    }

    public void setReducere(boolean reducere) {
        this.reducere = reducere;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNrTelefon() {
        return nrTelefon;
    }

    public void setNrTelefon(String nrTelefon) {
        this.nrTelefon = nrTelefon;
    }

    public String getTipUtilizator() {
        return tipUtilizator;
    }

    public void setTipUtilizator(String tipUtilizator) {
        this.tipUtilizator = tipUtilizator;
    }


    @Override
    public String toString() {
        return "Utilizator{" +
                "tipUtilizator='" + tipUtilizator + '\'' +
                ", nume='" + nume + '\'' +
                ", reducere=" + reducere +
                ", email='" + email + '\'' +
                ", nrTelefon='" + nrTelefon + '\'' +
                '}';
    }
}
