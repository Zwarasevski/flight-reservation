package Model.Persistenta;

import Model.Bilet;
import Model.Utilizator;

import java.io.*;
import java.util.ArrayList;

public class PersistentaBilet extends Persistenta {

    ArrayList<Bilet> bilete = new ArrayList<Bilet>();

    public PersistentaBilet(File numeFisier) {
        super(numeFisier);
        bilete = deserializare();
    }

    public void adauga(Bilet x){bilete.add(x);}
    public void sterge(Bilet x){bilete.add(x);}
    public ArrayList<Bilet> getBilete(){return bilete;}

    public void serializare(ArrayList<Bilet> x){
        try{
            FileOutputStream file = new FileOutputStream(numeFisier);
            ObjectOutputStream out = new ObjectOutputStream (file);
            out.writeObject(x);
            out.close();
            file.close();
        }
        catch (IOException ex) {
            System.out.println("IOException is caught");
        }
    }


    public  ArrayList<Bilet> deserializare(){
        try {
            FileInputStream file = new FileInputStream (numeFisier);
            ObjectInputStream in = new ObjectInputStream(file);
            bilete = (ArrayList<Bilet>) in.readObject();
            in.close();
            file.close();
            return bilete;
        }
        catch (IOException ex) {
            System.out.println("IOException is caught");
        }
        catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" +
                    " is caught");
        }
        return null;
    }

}
