package Model.Persistenta;

import Model.Cont;
import Model.Cursa;
import Model.Utilizator;

import java.io.*;
import java.util.ArrayList;

public class PersistentaAngajat extends Persistenta {

    private ArrayList<Utilizator> angajati = new ArrayList<>();


    public PersistentaAngajat(File numeFisier) {
        super(numeFisier);
        // trebuie sa verific daca fisierul nu e gol
         angajati = deserializareAngajati();
    }

    public void adaugaAngajat(Utilizator x){ angajati.add(x);}
    public void removeAngajat(Utilizator x) { angajati.remove(x);}
    public ArrayList<Utilizator> getAngajati(){ return angajati;}



    public void serializareAngajati(ArrayList<Utilizator> x){
        try{

            FileOutputStream file = new FileOutputStream(numeFisier);
            ObjectOutputStream out = new ObjectOutputStream (file);

            // Method for serialization of object
            out.writeObject(x);

            out.close();
            file.close();
            // System.out.println("A fost serializat");
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }
    }


    public  ArrayList<Utilizator> deserializareAngajati(){
        try {

            //ArrayList<Cursa> x = new ArrayList<>();
            // Reading the object from a file
            FileInputStream file = new FileInputStream (numeFisier);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialiazation of object

            angajati = (ArrayList<Utilizator>) in.readObject();

            in.close();
            file.close();
            //  System.out.println("Object has been deserialized\n"
            //return new String[]{cursa.getData(),cursa.getPlecare(),cursa.getSosire(),cursa.getDurata(),cursa.getAvion().getLinieAeriana(),cursa.getAvion().getNume(),Integer.toString(cursa.getAvion().getNumar())};
//           for(Cursa i:curse)
//              System.out.println(i.toString());

            return angajati;
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }

        catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" +
                    " is caught");
        }
        return null;
    }




}
