package Model.Persistenta;

import Model.Cursa;

import java.io.*;
import java.util.ArrayList;

public class PersistentaCursa extends Persistenta {
private ArrayList<Cursa> curse= new ArrayList<>();

    public PersistentaCursa(File numeFisier) {
        super(numeFisier);
        // trebuie sa verific daca fisierul nu e gol
       curse = deserializareArray();
    }


    public void stergeFisier(File file){
        try{
        new FileOutputStream(file).close();}
         catch (IOException ex) {
            System.out.println("IOException is caught");
        }
    }

    public File getFile(){ return numeFisier;}



    public void copiere(File in,File out){
        try{
        FileInputStream instream = new FileInputStream(in);
        FileOutputStream outstream = new FileOutputStream(out);
            byte[] buffer = new byte[1024];

            int length;
            /*copying the contents from input stream to
             * output stream using read and write methods
             */
            while ((length = instream.read(buffer)) > 0){
                outstream.write(buffer, 0, length);
            }

            //Closing the input/output file streams
            instream.close();
            outstream.close();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
    }


     public void incarcare(Cursa cursa){

        this.serializare(cursa);
        //this.serializare(cursa.getAvion());
     }

    public void serializare(Cursa cursa){
        try{

            // Saving of object in a file
            FileOutputStream file = new FileOutputStream(numeFisier);
            ObjectOutputStream out = new ObjectOutputStream (file);

            // Method for serialization of object
            out.writeObject(cursa);

            out.close();
            file.close();
           // System.out.println("A fost serializat");
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }

    }

    public void serializareArray(ArrayList<Cursa> x){
        try{

            FileOutputStream file = new FileOutputStream(numeFisier);
            ObjectOutputStream out = new ObjectOutputStream (file);

            // Method for serialization of object
            out.writeObject(x);

            out.close();
            file.close();
            // System.out.println("A fost serializat");
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }
    }
    public void serializareArray2(ArrayList<Cursa> x,File filename){
        try{

            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream (file);

            // Method for serialization of object
            out.writeObject(x);

            out.close();
            file.close();
            // System.out.println("A fost serializat");
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }
    }

    public  ArrayList<Cursa> deserializareArray(){
        try {

            //ArrayList<Cursa> x = new ArrayList<>();
            // Reading the object from a file
            FileInputStream file = new FileInputStream (numeFisier);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialiazation of object

            curse = (ArrayList<Cursa>) in.readObject();

            in.close();
            file.close();
            //  System.out.println("Object has been deserialized\n"
            //return new String[]{cursa.getData(),cursa.getPlecare(),cursa.getSosire(),cursa.getDurata(),cursa.getAvion().getLinieAeriana(),cursa.getAvion().getNume(),Integer.toString(cursa.getAvion().getNumar())};
//           for(Cursa i:curse)
//              System.out.println(i.toString());

            return curse;
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }

        catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" +
                    " is caught");
        }
        return null;
    }

    public ArrayList<Cursa> getCurse(){return curse;}
    public void adaugaCursa(Cursa x){ curse.add(x);}
    public void removeCursa(Cursa x){ curse.remove(x);}

    public String[] vizualizareCurse(Cursa cursa){
        try {

            // Reading the object from a file
            FileInputStream file = new FileInputStream (numeFisier);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialiazation of object

           cursa = (Cursa) in.readObject();

            in.close();
            file.close();
          //  System.out.println("Object has been deserialized\n"
            return new String[]{cursa.getData(),cursa.getPlecare(),cursa.getSosire(),cursa.getDurata(),cursa.getAvion().getLinieAeriana(),cursa.getAvion().getNume(),Integer.toString(cursa.getAvion().getNumar())};
          //  System.out.println(cursa.vizualizareOrar());
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }

        catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" +
                    " is caught");
        }
        return null;
    }
    public String[] vizualizareSosire(Cursa cursa){
        try {

            // Reading the object from a file
            FileInputStream file = new FileInputStream (numeFisier);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialiazation of object

            cursa = (Cursa) in.readObject();

            in.close();
            file.close();
            //  System.out.println("Object has been deserialized\n"
            return new String[]{cursa.getData(),Integer.toString(cursa.getPret()),Integer.toString(cursa.getAvion().getNrLocuri()),cursa.getDurata(),cursa.getAvion().getLinieAeriana(),cursa.getAvion().getNume(),Integer.toString(cursa.getAvion().getNumar())};
            //  System.out.println(cursa.vizualizareOrar());
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }

        catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" +
                    " is caught");
        }
        return null;
    }
    public String[] vizualizareAvion(Cursa cursa){
        try {

            // Reading the object from a file
            FileInputStream file = new FileInputStream (numeFisier);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialiazation of object

            cursa = (Cursa) in.readObject();

            in.close();
            file.close();
            //  System.out.println("Object has been deserialized\n"
            return new String[]{cursa.getAvion().getLinieAeriana(),cursa.getAvion().getNume(),Integer.toString(cursa.getAvion().getNumar())};
            //  System.out.println(cursa.vizualizareOrar());
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }

        catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" +
                    " is caught");
        }
        return null;
    }

    public String[] vizualizareAngajat(Cursa cursa){
        try {

            // Reading the object from a file
            FileInputStream file = new FileInputStream (numeFisier);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialiazation of object

            cursa = (Cursa) in.readObject();

            in.close();
            file.close();
            //  System.out.println("Object has been deserialized\n"
            return new String[]{Integer.toString(cursa.getPret()),cursa.getData(),cursa.getPlecare(),cursa.getSosire(),cursa.getDurata(),Integer.toString(cursa.getAvion().getNumar()),Integer.toString(cursa.getAvion().getNrLocuri()),cursa.getAvion().getNume(),cursa.getAvion().getLinieAeriana()};
           //   System.out.println(cursa.toString());
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }

        catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" +
                    " is caught");
        }
        return null;
    }
}
