package Model.Persistenta;

import Model.Avion;

import java.io.*;

public class Persistenta {

    protected File numeFisier;

    public Persistenta(File numeFisier) {
        this.numeFisier = numeFisier;
    }

    public File getNumeFisier() {
        return numeFisier;
    }

    public void setNumeFisier(File numeFisier) {
        this.numeFisier = numeFisier;
    }

}
