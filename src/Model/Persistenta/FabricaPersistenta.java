package Model.Persistenta;
import Model.Persistenta.*;

import java.io.File;

public class FabricaPersistenta {

    public Persistenta obtinePersistenta(String tipPersistenta, File filename){
        if(tipPersistenta.equals("Angajat")) return new PersistentaAngajat(filename);
        if(tipPersistenta.equals("Cont")) return new PersistentaCont(filename);
        if(tipPersistenta.equals("Cursa")) return new PersistentaCursa(filename);
        if(tipPersistenta.equals("Bilet")) return new PersistentaBilet(filename);
        return null;
    }
}
