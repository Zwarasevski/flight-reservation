package Model.Persistenta;

import Model.Cont;
import Model.Utilizator;

import java.io.*;
import java.util.ArrayList;

public class PersistentaCont extends Persistenta {
    private ArrayList<Cont> conturi= new ArrayList<>();
    public PersistentaCont(File numeFisier) {
        super(numeFisier);
        conturi=deserializareConturi();
    }



    public void adaugaCont(Cont x){conturi.add(x);}
    public void removeCont(Cont x){conturi.remove(x);}
    public ArrayList<Cont> getConturi (){ return conturi;}

    public String getParola(Utilizator x){
        for(Cont i:conturi)
            if(i.getEmail().equals(x.getEmail()))
                return i.getParola();
            return null;
    }
    public void serializareConturi(ArrayList<Cont> x){
        try{

            FileOutputStream file = new FileOutputStream(numeFisier);
            ObjectOutputStream out = new ObjectOutputStream (file);

            // Method for serialization of object
            out.writeObject(x);

            out.close();
            file.close();
            // System.out.println("A fost serializat");
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }
    }

    public  ArrayList<Cont> deserializareConturi(){
        try {

            //ArrayList<Cursa> x = new ArrayList<>();
            // Reading the object from a file
            FileInputStream file = new FileInputStream (numeFisier);
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialiazation of object

            conturi = (ArrayList<Cont>) in.readObject();

            in.close();
            file.close();
            //  System.out.println("Object has been deserialized\n"
            //return new String[]{cursa.getData(),cursa.getPlecare(),cursa.getSosire(),cursa.getDurata(),cursa.getAvion().getLinieAeriana(),cursa.getAvion().getNume(),Integer.toString(cursa.getAvion().getNumar())};
//           for(Cursa i:curse)
//              System.out.println(i.toString());

            return conturi;
        }

        catch (IOException ex) {
            System.out.println("IOException is caught");
        }

        catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" +
                    " is caught");
        }
        return null;
    }
}
