package Model;

import java.io.Serializable;

public class Bilet implements Serializable {
 private Cursa cursa;
 private String numeCalator;

    public Bilet(Cursa cursa, String numeCalator) {
        this.cursa = cursa;
        this.numeCalator = numeCalator;
    }

    public Cursa getCursa() {
        return cursa;
    }

    public void setCursa(Cursa cursa) {
        this.cursa = cursa;
    }

    public String getNumeCalator() {
        return numeCalator;
    }

    public void setNumeCalator(String numeCalator) {
        this.numeCalator = numeCalator;
    }

    public String[] vizualizare(){
        return new String[]{Integer.toString(cursa.getPret()), cursa.getData(), cursa.getPlecare(), cursa.getSosire(), cursa.getDurata(), cursa.getAvion().getLinieAeriana(),numeCalator};
    }
}
