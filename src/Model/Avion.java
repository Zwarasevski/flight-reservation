package Model;

import java.io.Serializable;
import java.util.Objects;

public class Avion implements Serializable {
    private static final long serialversionUID = 129348938L;
    private int numar;
    private int nrLocuri;
    private String nume;
    private String linieAeriana;


    public Avion(int numar, int nrLocuri, String nume, String linieAeriana) {
        this.numar = numar;
        this.nrLocuri = nrLocuri;
        this.nume = nume;
        this.linieAeriana = linieAeriana;
    }

    public int getNumar() {
        return numar;
    }

    public void setNumar(int numar) {
        this.numar = numar;
    }

    public int getNrLocuri() {
        return nrLocuri;
    }

    public void setNrLocuri(int nrLocuri) {
        this.nrLocuri = nrLocuri;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getLinieAeriana() {
        return linieAeriana;
    }

    public void setLinieAeriana(String linieAeriana) {
        this.linieAeriana = linieAeriana;
    }

    @Override
    public String toString() {
        return "Avion{" +
                "numar=" + numar +
                ", nrLocuri=" + nrLocuri +
                ", nume='" + nume + '\'' +
                ", linieAeriana='" + linieAeriana + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Avion avion = (Avion) o;
        return numar == avion.numar &&
                nrLocuri == avion.nrLocuri &&
                nume.equals(avion.nume) &&
                linieAeriana.equals(avion.linieAeriana);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numar, nrLocuri, nume, linieAeriana);
    }
}
