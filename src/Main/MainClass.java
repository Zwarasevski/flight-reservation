package Main;

import Controller.ControllerAdministrator;
import Controller.ControllerAngajat;
import Controller.ControllerAutentificare;
import Controller.ControllerCalator;
import Model.*;
import Model.Persistenta.*;
//import Model.Persistenta.PersistentaAvion;
import View.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class MainClass {

        public static void main(String[] args) throws IOException {


            File filename2 = new File("Angajati.txt");
            File filename3 = new File("Conturi.txt");
            File filename = new File("Orar.txt");
            File filename4 = new File("Bilete.txt");




            FabricaPersistenta fabricaPersistenta = new FabricaPersistenta();
            PersistentaAngajat persistentaAngajat = (PersistentaAngajat) fabricaPersistenta.obtinePersistenta("Angajat",filename2);
            PersistentaCont persistentaCont = (PersistentaCont) fabricaPersistenta.obtinePersistenta("Cont",filename3);
            PersistentaCursa persistentaCursa = (PersistentaCursa) fabricaPersistenta.obtinePersistenta("Cursa",filename);
            PersistentaBilet persistentaBilet = (PersistentaBilet) fabricaPersistenta.obtinePersistenta("Bilet",filename4);

            // scriere CSV File
            FileWriter writer = new FileWriter("DateCurse.csv");
            String[][] x = new String[persistentaCursa.getCurse().size()][7];
            int j = 0;
            for(Cursa i: persistentaCursa.getCurse()){
                x[j] =  i.vizualizareOrar();
                CSVUtils.writeLine(writer, Arrays.asList(x[j][0],x[j][1], x[j][2], x[j][3],x[j][4],x[j][5],x[j][6]));
                j++;
            }
            writer.flush();
            writer.close();



            ControllerAngajat controllerAngajat = new ControllerAngajat(persistentaCursa,persistentaBilet);
            ControllerCalator calator = new ControllerCalator(persistentaCursa);
            ControllerAdministrator controllerAdministrator = new ControllerAdministrator(persistentaAngajat,persistentaCont);
            ControllerAutentificare controllerAutentificare = new ControllerAutentificare(persistentaAngajat,persistentaCont);

            ViewCalator viewCalator  = new ViewCalator(calator);
            ViewAngajat viewAngajat =new ViewAngajat(controllerAngajat);
            ViewAdministrator viewAdministrator = new ViewAdministrator(controllerAdministrator);
            ViewAutentificare viewAutentificare1 = new ViewAutentificare(controllerAutentificare);
            

            JFrame frame= new JFrame("RezervareBilete");

            frame.setSize(800,800);
            frame.setBackground(Color.BLUE);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            JTabbedPane tabbedPane = new JTabbedPane();
            tabbedPane.addTab("Calator",null,viewCalator,"Aici esti calator");

            tabbedPane.addTab("Angajat",null, viewAngajat,"Aici esti angajat");
            tabbedPane.addTab("Administrator",null,viewAdministrator,"Aici esti administrator");



            tabbedPane.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent changeEvent) {
                    if(tabbedPane.getSelectedIndex()==1){
                        viewAutentificare1.setVisible(true);
                        viewAutentificare1.verificareAngajat();

                    }
                    if(tabbedPane.getSelectedIndex()==2){
                        viewAutentificare1.setVisible(true);
                        viewAutentificare1.verificareAdministrator();
                    }

                }
            });
            frame.add(tabbedPane);
            frame.pack();
            frame.setVisible(true);

        }
    }


