package View;

import Controller.ControllerAdministrator;
import Model.Cont;
import Model.Utilizator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewAdministrator extends JPanel {

    ControllerAdministrator controllerAdministrator;

    String[] columnNames = {"Nume","Reducere","Email","Telefon","Parola"};
    Object[][] data = null;
    private JLabel l = new JLabel("Administrator");
    private JPanel p1 = new JPanel();
    private JPanel p2 = new JPanel();

    private JLabel l1 = new JLabel("Nume");
    private JTextArea text1 = new JTextArea("text1");
    private JLabel l2 = new JLabel("Reducere");
    private JTextArea text2 = new JTextArea("text2");

    private JLabel l3 = new JLabel("Email");
    private JTextArea text3 = new JTextArea("text3");
    private JLabel l4 = new JLabel("Telefon");
    private JTextArea text4 = new JTextArea("text4");
    private JLabel l5 = new JLabel("Parola Cont");
    private JTextArea text5 = new JTextArea("text5");
    private JTable tabel;
    private  JScrollPane scrollPane ;

    private JButton buton1 = new JButton("Adauga angajat");
   // private JButton buton2 = new JButton("Actualizeaza angajat");
    private JButton buton3 = new JButton("Sterge angajat");
    private JButton buton4 = new JButton("Vizualizare angajati");


    public ViewAdministrator(ControllerAdministrator controllerAdministrator){

        this.controllerAdministrator = controllerAdministrator;
        GridLayout layout = new GridLayout(0, 1, 0, 5);
//        setLayout(layout);
        p1.setLayout(layout);
        //p1.add(l);
        p1.add(l1);
        p1.add(text1);
        p1.add(l2);
        p1.add(text2);
        p1.add(l3);
        p1.add(text3);
        p1.add(l4);
        p1.add(text4);
        p1.add(l5);
        p1.add(text5);
        p1.add(buton1);
       // p1.add(buton2);
        p1.add(buton3);
        p1.add(buton4);

        DefaultTableModel def = new DefaultTableModel(data,columnNames);
        def.setRowCount(0);
        tabel= new JTable(def);
        scrollPane = new JScrollPane(tabel);
        tabel.setFillsViewportHeight(true);
        add(p1);
        add(scrollPane);

        buton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                def.addRow(controllerAdministrator.adaugaAngajat(new Utilizator("Angajat", text1.getText(),Boolean.parseBoolean(text2.getText()),text3.getText(),text4.getText()),new Cont(text3.getText(),text5.getText())));
            }
        });

        buton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                def.setRowCount(0);
                for(int i = 0;i<controllerAdministrator.vizualizare().length;i++)
                    def.addRow(controllerAdministrator.vizualizare()[i]);
            }
        });

        buton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                def.removeRow(tabel.getSelectedRow());
            }
        });

    }
}
