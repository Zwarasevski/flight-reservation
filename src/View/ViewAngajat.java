package View;

import Controller.ControllerAngajat;
import Model.Avion;
import Model.Bilet;
import Model.Cursa;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class ViewAngajat extends JPanel {
    ControllerAngajat controllerAngajat;
    String[] columnNames = {"pret","data","plecare","sosire","durata","numar avion", "numar locuri", "nume","linie aeriana"};
    Object[][] data = null;
    String[] columnNames2 = {"pret","data","plecare","sosire","durata","linie aeriana","nume calator"};
    Object[][] data2 = null;
    private JLabel l = new JLabel("Angajat");
    private JPanel p1 = new JPanel();
    private JPanel p2 = new JPanel();
    private JPanel p3 = new JPanel();
    private JPanel p4 = new JPanel();
    private JPanel p5 = new JPanel();
    private JPanel p6 = new JPanel();
    private JPanel p7 = new JPanel();

    private JPanel p9 = new JPanel();
    private JPanel p8 = new JPanel();

    private JLabel l1 = new JLabel("Pret");
    private JTextArea text1 = new JTextArea("text1");
    private JLabel l2 = new JLabel("Data");
    private JTextArea text2 = new JTextArea("text2");

    private JLabel l3 = new JLabel("Plecare");
    private JTextArea text3 = new JTextArea("text3");
    private JLabel l4 = new JLabel("Sosire");
    private JTextArea text4 = new JTextArea("text4");
    private JLabel l5 = new JLabel("Durata");
    private JTextArea text5 = new JTextArea("text5");

    private JLabel l6 = new JLabel("Numar avion");
    private JTextArea text6 = new JTextArea("text6");
    private JLabel l7 = new JLabel("Numar Locuri");
    private JTextArea text7 = new JTextArea("text7");
    private JLabel l8 = new JLabel("Nume");
    private JTextArea text8 = new JTextArea("text8");
    private JLabel l9 = new JLabel("Linie aeriana");
    private JTextArea text9 = new JTextArea("text9");

    private JButton buton1 = new JButton("Adauga cursa");

    private JLabel l10 = new JLabel("Nume Calator");
    private JTextArea text10 = new JTextArea("text10");


    private JButton buton2 = new JButton("Vinde bilet");

    //private JButton buton3 = new JButton("Actualizeaza Cursa");
    private JButton buton4 = new JButton("Sterge Cursa");
    private JButton buton5 = new JButton("Vizualizeaza Curse");

//    private JButton buton6 = new JButton("Actualizeaza bilet");
    private JButton buton7 = new JButton("Sterge bilet");
    private JButton buton8 = new JButton("Vizualizeaza bilete");
  //  private JButton buton3 = new JButton("Cauta avion");
    private JTable tabel ;
    private JTable tabel2 ;
    private  JScrollPane scrollPane ;
    private  JScrollPane scrollPane2 ;
    private DefaultTableModel def = new DefaultTableModel(data,columnNames);
    private  DefaultTableModel def2 = new DefaultTableModel(data2,columnNames2);

    public ViewAngajat(ControllerAngajat controllerAngajat){
        this.controllerAngajat = controllerAngajat;

        createGUI();
        buton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                def.addRow(controllerAngajat.adaugaCursa(new Cursa(Integer.parseInt(text1.getText()),text2.getText(),text3.getText(),text4.getText(),text5.getText(),new Avion(Integer.parseInt(text6.getText()),Integer.parseInt(text7.getText()),text8.getText(),text9.getText()))));
            }
        });
//
//        buton3.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent actionEvent) {
//
//                Vector x =def.getDataVector().elementAt(tabel.getSelectedRow());
//                Avion avion = new Avion(Integer.parseInt((String) x.get(5)),Integer.parseInt((String) x.get(6)),(String)x.get(7),(String)x.get(8));
//                Cursa cursa = new Cursa(Integer.parseInt((String) x.get(0)),(String) x.get(1),(String)x.get(2),(String)x.get(3),(String)x.get(4),avion);
//                int j=tabel.getSelectedRow();
//                controllerAngajat.actualizeaza(cursa,j);
//                for(int i = 0;i<controllerAngajat.vizualizare().length;i++)
//                    def.addRow(controllerAngajat.vizualizare()[i]);
//            }
//        });

        buton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Vector x =def.getDataVector().elementAt(tabel.getSelectedRow());
                Avion avion = new Avion(Integer.parseInt((String) x.get(5)),Integer.parseInt((String) x.get(6)),(String)x.get(7),(String)x.get(8));
                Cursa cursa = new Cursa(Integer.parseInt((String) x.get(0)),(String) x.get(1),(String)x.get(2),(String)x.get(3),(String)x.get(4),avion);
                def2.addRow(controllerAngajat.adaugaBilet(new Bilet(cursa,text10.getText())));
                //def2.addRow(x2);
              //  def2.addRow(x);
            }
        });

        buton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
              //  controllerAngajat.stergeCursa();
                def.removeRow(tabel.getSelectedRow());
//                Vector x =def.getDataVector().elementAt(tabel.getSelectedRow());
//                Avion avion = new Avion(Integer.parseInt((String) x.get(5)),Integer.parseInt((String) x.get(6)),(String)x.get(7),(String)x.get(8));
//                Cursa cursa = new Cursa(Integer.parseInt((String) x.get(0)),(String) x.get(1),(String)x.get(2),(String)x.get(3),(String)x.get(4),avion);
//
//                def.setRowCount(0);
//                for(int i = 0;i<controllerAngajat.stergeCursa(cursa).length;i++)
//                    def.addRow(controllerAngajat.stergeCursa(cursa)[i]);
               // controllerAngajat.stergeCursa(new Cursa(Integer.parseInt(text1.getText()),text2.getText(),text3.getText(),text4.getText(),text5.getText(),new Avion(Integer.parseInt(text6.getText()),Integer.parseInt(text7.getText()),text8.getText(),text9.getText())));
            }
        });
        buton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                def.setRowCount(0);
                for(int i = 0;i<controllerAngajat.vizualizare().length;i++)
                    def.addRow(controllerAngajat.vizualizare()[i]);
            }
        });

        buton7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                def2.removeRow(tabel2.getSelectedRow());
            }
        });
        buton8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                def2.setRowCount(0);
                for(int i = 0;i<controllerAngajat.vizualizareBilete().length;i++)
                    def2.addRow(controllerAngajat.vizualizareBilete()[i]);
            }
        });

    }

    public void createGUI(){
        GridLayout layout = new GridLayout(0, 1, 0, 5);
        //setLayout(layout);
        p1.setLayout(layout);
        p9.setLayout(layout);
        //p3.setLayout(layout);


        p1.add(l1);
        p1.add(text1);

        p1.add(l2);
        p1.add(text2);

        p1.add(l3);
        p1.add(text3);

        p1.add(l4);
        p1.add(text4);

        p1.add(l5);
        p1.add(text5);

        //avion
        p1.add(l6);
        p1.add(text6);

        p1.add(l7);
        p1.add(text7);

        p1.add(l8);
        p1.add(text8);

        p1.add(l9);
        p1.add(text9);

        p1.add(buton1);
      //  p1.add(buton3);
        p1.add(buton4);
        p1.add(buton5);



        def.setRowCount(0);
        tabel= new JTable(def);
        scrollPane = new JScrollPane(tabel);
        tabel.setFillsViewportHeight(true);
        p4.add(scrollPane);

//        p8.add(p1);
//        p8.add(p2);
//        p8.add(p3);
//        p8.add(p4);

        p9.add(l10);
        p9.add(text10);
        p9.add(buton2);
        p9.add(buton7);
        p9.add(buton8);


        // p9.add(p6);

        def2.setRowCount(0);
        tabel2 = new JTable(def2);
        scrollPane2 = new JScrollPane(tabel2);
        tabel2.setFillsViewportHeight(true);
        p7.add(scrollPane2);
        //  p9.add(p5);
//        p9.add(p7);

        add(p1);
        add(p4);
        add(p9);
        add(p7);
    }


}