package View;
import Controller.ControllerAutentificare;
import Model.Cont;

import javax.swing.*;

public class ViewAutentificare extends JFrame {
    private ControllerAutentificare controllerAutentificare;
    private String email = new String();
    private String pass = new String();

    public ViewAutentificare(ControllerAutentificare x){
        controllerAutentificare = x;
  }

  public void verificareAngajat(){
      email= JOptionPane.showInputDialog("Email");
      pass = JOptionPane.showInputDialog("Parola");
      if (controllerAutentificare.verificareAngajat(new Cont(email, pass)) == true) {}
      else {JOptionPane.showMessageDialog(this,"Nu exista acest cont");
      verificareAngajat();}
  }

  public void verificareAdministrator(){
      email= JOptionPane.showInputDialog("Email");
      pass = JOptionPane.showInputDialog("Parola");
        if(controllerAutentificare.verificareAdministrator(new Cont(email,pass))==true) {}
        else {JOptionPane.showMessageDialog(this,"Nu exista acest cont");
            verificareAdministrator();}
  }
}
