package View;

import Controller.ControllerCalator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewCalator extends JPanel {
    ControllerCalator controllerCalator;
    String[] columnNames = {"data","plecare","sosire","durata","linie aeriana", "nume", "numar avion"};
    Object[][] data = null;
    //private JTable tabel;
    private JPanel p1 = new JPanel();
    private JPanel p2 = new JPanel();
    private JPanel p3 = new JPanel();
    private JPanel p4 = new JPanel();
    private JPanel p5 = new JPanel();
    private JPanel p6 = new JPanel();
    private JPanel p7 = new JPanel();
    private JButton buton1 = new JButton("Vizualizare curse");
    private JLabel l1 = new JLabel("statie plecare");
    private JTextArea text1 = new JTextArea("text1");
    private JLabel l2 = new JLabel("statie sosire");
    private JTextArea text2 = new JTextArea("text2");
    private JButton buton2 = new JButton("Cauta");
    private JLabel l3 = new JLabel("Numar avion");
    private JTextArea text3 = new JTextArea("numar");
    private JButton buton3 = new JButton("Cauta avion");
    private JTable tabel ;
    private JTable tabel2 ;
    private  JScrollPane scrollPane ;
    private  JScrollPane scrollPane2 ;


    public ViewCalator(ControllerCalator controllerCalator){
        this.controllerCalator = controllerCalator;

        GridLayout layout = new GridLayout(0, 1, 0, 5);
//        setLayout(layout);
        this.setSize(800,800);
        p1.setLayout(layout);
        this.add(p1);
        this.add(p2);
        p1.add(buton1);
        p5.add(l1);
        p5.add(text1);
        p1.add(p5);
        p3.add(l2);
        p3.add(text2);
        p1.add(p3);
        p1.add(buton2);
        p4.add(l3);
        p4.add(text3);
        p1.add(p4);
        p1.add(buton3);
        DefaultTableModel def = new DefaultTableModel(data,columnNames);
        def.setRowCount(0);
        tabel= new JTable(def);
        scrollPane = new JScrollPane(tabel);
        tabel.setFillsViewportHeight(true);
        p2.add(scrollPane);
        buton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String[] columnNames = {"data","plecare","sosire","durata","linie aeriana", "nume", "numar avion"};
                def.setColumnCount(0);
                for(int i=0;i<7;i++)
                    def.addColumn(columnNames[i]);
                def.setRowCount(0);
                for(int i = 0;i<controllerCalator.vizualizare().length;i++)
                    def.addRow(controllerCalator.vizualizare()[i]);
            }
        });
        buton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String[] columnNames1 = {"data","pret","locuri disponibile","durata","linie aeriana", "nume", "numar avion"};
                def.setColumnCount(0);
                for(int i=0;i<7;i++)
                    def.addColumn(columnNames1[i]);
                def.setRowCount(0);
                for(int i =0;i<controllerCalator.sosirePlecare(text2.getText(),text1.getText()).length;i++)
                    def.addRow(controllerCalator.sosirePlecare(text2.getText(),text1.getText())[i]);
                //controllerCalator.sosirePlecare(text2.getText(),text1.getText());
            }
        });

        buton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String[] columnNames2 = {"linie aeriana", "nume", "numar avion"};
                def.setColumnCount(0);
                for(int i=0;i<3;i++)
                    def.addColumn(columnNames2[i]);
                def.setRowCount(0);
                def.addRow(controllerCalator.cautaAvion(Integer.parseInt(text3.getText())));

            }
        });
    }

}
