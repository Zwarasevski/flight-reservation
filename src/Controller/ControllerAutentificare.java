package Controller;

import Model.Cont;
import Model.Persistenta.Persistenta;
import Model.Persistenta.PersistentaAngajat;
import Model.Persistenta.PersistentaCont;
import Model.Utilizator;

import java.util.ArrayList;

public class ControllerAutentificare {
//    private ArrayList<Utilizator>  utilizatori;
//    private ArrayList<Cont> conturi;

    PersistentaAngajat persistentaAngajat;
    PersistentaCont persistentaCont;
    public ControllerAutentificare(PersistentaAngajat x, PersistentaCont y) {
        persistentaAngajat = x;
        persistentaCont = y;
    }

    public boolean verificareAngajat(Cont x){
        int ok=0;
        for(Cont i: persistentaCont.getConturi()){
            if(i.getEmail().equals(x.getEmail()) && i.getParola().equals(x.getParola())) ok= 1;
        }
        if(ok==1)
            for(Utilizator i: persistentaAngajat.getAngajati()){
                if(i.getEmail().equals(x.getEmail()) && (i.getTipUtilizator().equals("Angajat"))) return true;
            }
      return false;
    }

    public boolean verificareAdministrator(Cont x){
        if(x.getEmail().equals("rominazwarasevski@yahoo.com")&& x.getParola().equals("pass")) return true;
        return false;
    }
}
