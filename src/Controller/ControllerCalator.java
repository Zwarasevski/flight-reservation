package Controller;
import Model.*;
import Model.Persistenta.PersistentaCursa;

import java.util.ArrayList;

public class ControllerCalator implements ControllerBuiler {
   // private Orar orar;
    //ArrayList<Cursa> curse;
    PersistentaCursa persistentaCursa;

    public ControllerCalator( PersistentaCursa persistentaCursa) {
       // this.curse = curse;
        this.persistentaCursa = persistentaCursa;
    }

//    public String[][] vizualizareOrar(){
//        String[][] x = new String[persistentaCursa.getCurse().size()][7];
//        int j = 0;
////        for(Cursa i: persistentaCursa.deserializareArray())
////            System.out.println(i.toString());
//        for(Cursa i: persistentaCursa.getCurse()){
//               // persistentaCursa.incarcare(i);
//              x[j] =  i.vizualizareOrar();
//              j++;
//            }
//        return x;
//    }

    public String[][] sosirePlecare(String sosire,String plecare){
        String[][] x = new String[persistentaCursa.getCurse().size()][7];
        int j = 0;
        for(Cursa i: persistentaCursa.getCurse()) {
                persistentaCursa.incarcare(i);
                if(i.getPlecare().equals(plecare) && i.getSosire().equals(sosire))
                    x[j] =  persistentaCursa.vizualizareSosire(i);
                    j++;
            }
        return x;
    }

    public String[] cautaAvion(int numar){
        for(Cursa i: persistentaCursa.getCurse()) {
            persistentaCursa.incarcare(i);
            if(i.getAvion().getNumar() == numar)
               return persistentaCursa.vizualizareAvion(i);
        }
        return null;
    }

    @Override
    public String[][] vizualizare() {
        String[][] x = new String[persistentaCursa.getCurse().size()][7];
        int j = 0;
//        for(Cursa i: persistentaCursa.deserializareArray())
//            System.out.println(i.toString());
        for(Cursa i: persistentaCursa.getCurse()){
            // persistentaCursa.incarcare(i);
            x[j] =  i.vizualizareOrar();
            j++;
        }
        return x;
    }
}
