package Controller;
import Model.Bilet;
import Model.Cursa;
import Model.Persistenta.PersistentaBilet;
import Model.Persistenta.PersistentaCursa;

import java.io.File;
import java.util.ArrayList;

public class ControllerAngajat implements ControllerBuiler{

    private PersistentaCursa persistentaCursa;
    private PersistentaBilet persistentaBilet;

    public ControllerAngajat(PersistentaCursa x,PersistentaBilet y) {
        persistentaCursa = x;
        persistentaBilet = y;
    }

    public String[] adaugaCursa(Cursa cursa){
        persistentaCursa.adaugaCursa(cursa);
        persistentaCursa.serializareArray(persistentaCursa.getCurse());
        return cursa.vizualizareAngajat();
    }

    public String[] adaugaBilet(Bilet bilet){
        persistentaBilet.adauga(bilet);
        persistentaBilet.serializare(persistentaBilet.getBilete());
        return bilet.vizualizare();
    }

    public void actualizeaza(Cursa cursa,int x){
        int j=0;
        for(Cursa i:persistentaCursa.getCurse()){
            if(j==x){
               i.setCursa(cursa);
            }
        j++;
        }

        persistentaCursa.serializareArray(persistentaCursa.getCurse());

    }

//    public void stergeCursa(Cursa cursa){
//
//        File temp2 = new File("temp2.txt");
//
//       persistentaCursa.removeCursa(cursa);
//        ArrayList<Cursa> temp = persistentaCursa.getCurse();
//        persistentaCursa.stergeFisier(persistentaCursa.getNumeFisier());
//        persistentaCursa.serializareArray2(temp,temp2);
//        persistentaCursa.copiere(temp2,persistentaCursa.getFile());
//        temp2.delete();

//        persistentaCursa.serializareArray(temp);
//        String[][] x = new String[persistentaCursa.getCurse().size()][7];
//        int j = 0;
//        for(Cursa i: persistentaCursa.getCurse()){
//            x[j] =  i.vizualizareOrar2();
//            j++;
//        }
//        return x;
       // return vizualizare();
//        //return persistentaCursa.
//    }

//    public ArrayList<Cursa> getOrar(){
//        return persistentaCursa.getCurse();


    @Override
    public String[][] vizualizare() {
        String[][] x = new String[persistentaCursa.getCurse().size()][7];
        int j = 0;
        for(Cursa i: persistentaCursa.getCurse()){
            x[j] =  i.vizualizareOrar2();
            j++;
        }
        return x;
    }

    public String[][] vizualizareBilete() {
        String[][] x = new String[persistentaBilet.getBilete().size()][7];
        int j = 0;
        for(Bilet i: persistentaBilet.getBilete()){
            x[j] =  i.vizualizare();
            j++;
        }
        return x;
    }
}
