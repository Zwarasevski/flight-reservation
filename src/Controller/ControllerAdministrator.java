package Controller;
import Model.*;
import Model.Persistenta.*;


public class ControllerAdministrator implements ControllerBuiler{

    private PersistentaAngajat persistentaAngajat;
    private PersistentaCont persistentaCont;


    public ControllerAdministrator(PersistentaAngajat persistentaAngajat, PersistentaCont persistentaCont) {
        this.persistentaAngajat = persistentaAngajat;
        this.persistentaCont = persistentaCont;
    }

    public String[] adaugaAngajat(Utilizator x, Cont cont){
        persistentaAngajat.adaugaAngajat(x);
        persistentaCont.adaugaCont(cont);
        persistentaAngajat.serializareAngajati(persistentaAngajat.getAngajati());
        persistentaCont.serializareConturi(persistentaCont.getConturi());
        return new String[]{x.getNume(),String.valueOf(x.isReducere()),x.getEmail(),x.getNrTelefon(),cont.getParola()};

    }

    @Override
    public String[][] vizualizare() {
        String[][] x = new String[persistentaAngajat.getAngajati().size()][5];
        int j = 0;
        for(Utilizator i: persistentaAngajat.getAngajati()){
            x[j] = new String[]{i.getNume(),String.valueOf(i.isReducere()),i.getEmail(),i.getNrTelefon(),persistentaCont.getParola(i)};
            j++;
        }
        return x;
    }
}
